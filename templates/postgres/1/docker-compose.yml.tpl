version: '2'
volumes:
  pgdata:
    driver: ${VOLUME_DRIVER}    
services:
  postgres-data:
    image: busybox
    labels:
      io.rancher.container.start_once: true
    volumes:
      - pgdata:/var/lib/postgresql/data/pgdata  
  postgres:
    image: postgres:${POSTGRES_TAG}
    environment:
      PGDATA: "/var/lib/postgresql/data/pgdata"
      POSTGRES_DB: "${postgres_db}"
      POSTGRES_USER: "${postgres_user}"
      POSTGRES_PASSWORD: "${postgres_password}"
    tty: true
    stdin_open: true
    labels:
      io.rancher.sidekicks: postgres-data
    volumes_from:
      - postgres-data

